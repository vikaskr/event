<?php

namespace app\migrations;
use app\commands\Migration;

class m170117_163608_create_post_ping_stats extends Migration
{
    public function getTableName()
    {
        return 'post_ping_stats';
    }
    public function getForeignKeyFields()
    {
        return [
            'post_id' => ['post', 'id'],
            'ping_id' => ['mub_ping_urls','id']
        ];
    }

    public function getKeyFields()
    {
        return [
            'post_id' => 'post_id',
            'ping_id'  =>  'ping_id',
            'status' => 'status'
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'post_id' => $this->integer()->defaultValue(NULL),
            'ping_id' => $this->integer()->defaultValue(NULL),
            'status' => "enum('pinged','queued','waiting','initial') NOT NULL DEFAULT 'initial'",
            'response_text' => $this->string(),
            'created_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}

<?php

namespace app\migrations;
use app\commands\Migration;

class m170828_080100_calculator_form extends Migration
{
        public function getTableName()
    {
        return 'calculator_form';
    }

    public function getKeyFields()
    {
        return [
                'name' => 'name',
                'email' => 'email',
                'phone' => 'phone',
                'plant' => 'plant',
                'location' => 'location',
                ];
    }

    public function getFields()
    {
        return [
        'id' => $this->primaryKey(),
            'name' => $this->string(50)->Null(),
            'email' => $this->string(50)->Null(),
            'phone' => $this->string(50)->Null(),
            'plant' => $this->string(50)->Null(),
            'location' => $this->string(50)->Null(),
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'"
        ];
    }
}

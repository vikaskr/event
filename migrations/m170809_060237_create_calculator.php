<?php

namespace app\migrations;
use app\commands\Migration;

class m170809_060237_create_calculator extends Migration
{
    
    public function getTableName()
    {
        return 'calculator_details';
    }

    public function getKeyFields()
    {
        return [
                'name' => 'name',
                'email' => 'email',
                'mobile' => 'mobile',
                'saving' => 'saving',
                'language' => 'language',
                'leaked' => 'leaked',
                'cpm' => 'cpm',
                'hours' => 'hours',
                'kw' => 'kw'
                ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'name' => $this->string(50),
            'email' => $this->string(50)->notNull(),
            'mobile' => $this->string(50),
            'saving' => $this->string(50),
            'language' => $this->string(50),
            'leaked' => $this->string(50),
            'cpm' => $this->string(50),
            'hours' => $this->string(50),
            'kw' => $this->string(50),
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'"
        ];
    }
}

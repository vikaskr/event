<?php

namespace app\migrations;
use app\commands\Migration;

class m170117_163424_create_mub_category extends Migration
{
    public function getTableName()
    {
        return 'mub_category';
    }

    public function getForeignKeyFields()
    {
        return [
            'category_parent' => ['mub_category','id']
        ];
    }

    public function getKeyFields()
    {
        return [
            'category_name' => 'category_name',
            'category_slug' => 'category_slug'
        ];
    }

    public function safeUp()
    {
        parent::safeUp();
        $mubCategory = new \app\models\MubCategory();
        $mubCategory->category_name = 'default';
        $mubCategory->category_slug = 'default';
        if(!$mubCategory->save())
        {
            p($mubCategory->getErrors());
        }
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'category_name' => $this->string(50)->notNull(),
            'category_slug' => $this->string(50)->notNull(),
            'category_parent' => $this->integer(11),
            'category_child' => $this->integer(11),
            'created_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'", 
        ];
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_states".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property integer $state_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 * @property MubUser $mubUser
 * @property State $state
 */
class UserStates extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_states';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['state_id'],'required'],
            [['mub_user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['del_status'], 'string'],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
        ];
    }

     public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create_mub_user'] = ['state_id'];
        $scenarios['update_mub_user'] = ['state_id'];
        $scenarios['status_update'] = ['status'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'state_id' => 'State ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(State::className(), ['id' => 'state_id']);
    }
}

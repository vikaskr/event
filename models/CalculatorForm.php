<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "calculator_form".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $plant
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 */
class CalculatorForm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'calculator_form';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'],'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['del_status'], 'string'],
            ["email","email"],
            ['phone','number'],
            ['phone','string', 'max' => 15 , 'min' => 6],
            [['name', 'email', 'plant','location'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'plant' => 'Plant Name',
            'location' => 'Plant Location',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }
}

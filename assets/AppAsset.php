<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '/css/bootstrap.min.css',
        '/css/font-awesome/css/font-awesome.min.css',
        '/css/color.css',
        '/css/style.css',

    ];
    public $js = [
        '/js/modernizr.custom.js',
        '/js/jquery.min.js',
        'http://maps.google.com/maps/api/js?sensor=false',
        '/js/directions.js',
        '/js/bootstrap.min.js',
        '/js/placeholders.js',
        '/js/jquery.superslides.min.js',
        '/js/jquery.countdown.js',
        '/js/owl.carousel.min.js',
        '/js/custom.js',
        '/js/master.js',
        
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $postComment app\postComments\PostComment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-comment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($postComment, 'post_id')->dropDownList($posts) ?>

    <?= $form->field($postComment, 'name')->textInput(['maxlength' => true,'value' => $user->first_name.' '.$user->last_name]); ?>

     <?= $form->field($postComment, 'email')->textInput(['maxlength' => true,'value' => $userContact->email]); ?>

    <?= $form->field($postComment, 'title')->textInput(['maxlength' => true])->label('Comment Title'); ?>

    <?= $form->field($postComment, 'type')->dropDownList([ '0' => 'Normal', '1'=> 'Reply', ], ['prompt' => 'Comment Type']) ?>

    <?= $form->field($postComment, 'comment_text')->textarea(['rows' => 6]) ?>

    <?= $form->field($postComment, 'status')->dropDownList([ '0', '1', '2', '3', '4', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($postComment->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $postComment->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

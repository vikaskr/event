<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $postComment app\postComments\PostComment */

	$this->title = Yii::t('app', 'Add A Comment');
	$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Post Comments'), 'url' => ['index']];
	$this->params['breadcrumbs'][] = $this->title;
	$user = new \app\models\MubUser();
	$currentUser = $user::find()->where(['user_id' => \Yii::$app->user->id])->one();
	$userContact = $currentUser->mubUserContacts;
?>
<div class="post-comment-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'postComment' => $postComment,
        'posts' => $posts,
        'user' => $currentUser,
        'userContact' => $userContact
    ]) ?>

</div>

<div class="x_panel">
  <div class="x_title">
    <h2>Your Dashboard</h2>
    <ul class="nav navbar-right panel_toolbox">
      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
      </li>
      <li><a class="close-link"><i class="fa fa-close"></i></a>
      </li>
    </ul>
    <div class="clearfix"></div>
  </div>
  <div class="x_content">
    <h1>Welcome to your Core3Online Dashboard</h1><br />
    <div>
      <div class="starrr stars"></div>
      <h3>1. Here You can add/view Services.</h3>
    </div>
 
    <h3>2. From The left Sidebar You can select the Values to be displayed.</h3>
    
    <h3>3. By Clicking on the profile section you will be able to update your Information.</h3>
    <br />
  </div>
</div>
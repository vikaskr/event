<?php 
use app\helpers\ImageUploader;
use yii\widgets\ActiveForm;
	$postModel = new \app\models\Post();
	$postImages = new \app\models\PostImages();
	$postImage = $postImages::find()->where(['post_id' => $pageData['post_id']])->one();
	$postComment = new \app\models\PostComment();
	$signupMail = new \app\models\SignupMail();
?>

<div class="technology-1">
	<div class="container">
		<div class="col-md-9 technology-left">
			<div class="business">
				<div class=" blog-grid2">
				<?php if($postImage){?>
					<img src="<?= ($postImage) ? '/'.ImageUploader::resizeRender($postImage->url, '650', '300') : 'https://placeholdit.imgix.net/~text?txtsize=61&txt=650%C3%97300&w=650&h=300';?>" class="img-responsive" alt="">
					<?php }?>
					<div class="blog-text">
						<h5><?=$pageData['title'];?></h5>
						<?= $pageData['content']; ?>			
					</div>
				</div>
				<?php if($comments){?>
				<h2>Comments</h2>
				<?php foreach($comments as $cmt){?>
				<div class="comment-top">
				<div class="media-left">
				  <a href="#">
					<img src="/images/si.png" alt="" data-pin-nopin="true">
				  </a>
				</div>
					<div class="media-body">
					  	<h4 class="media-heading"><?=$cmt->name;?></h4>
						<p class="show-less<?=$cmt->id;?>"><?php if(strlen($cmt->comment_text) > 255){
							echo substr($cmt->comment_text, 0,250); ?>
							<a style="cursor: pointer;" onclick="readmore(<?=$cmt->id;?>)"> read more...</a>
							<?php } else{
								echo $cmt->comment_text;
								}?></p>
								<p class="show-more<?=$cmt->id;?>" style="display:none;">
									<?=$cmt->comment_text;?>
									<a style="cursor: pointer;" onclick="readless(<?=$cmt->id;?>)"> show less...</a>
								</p>
					</div>
				</div>
					<?php }?>
				<?php }?>
				<div class="comment">
					<h3>Leave a Comment</h3>
					<div class=" comment-bottom">
					<?php $form = ActiveForm::begin(['id' => 'user_comment']);?>
							<?= $form->field($postComment, 'name')->textInput(['maxlength'=> 250]) ?>
							<?= $form->field($postComment, 'email')->textInput(['maxlength'=> 250]) ?>
							<?= $form->field($postComment, 'comment_text')->textArea(['cols'=>'6']) ?>
							<?= $form->field($postComment, 'url')->hiddenInput(['value' => \Yii::$app->request->get('id')])->label(false) ?>
							<input type="submit" value="Post">
						<?php ActiveForm::end();?>
					</div>
				</div>
			</div>
		</div>
		<!-- technology-right -->
		<?php echo $this->render('_right',['postModel' => $postModel]);?>
		<div class="clearfix"></div>
		<!-- technology-right -->
	</div>
</div>
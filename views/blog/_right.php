<?php 

use app\helpers\ImageUploader;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
    $postImages = new \app\models\PostImages();
    $signupMail = new \app\models\SignupMail();
    $postComment = new \app\models\PostComment();

      $allPosts = $postModel::find()->where(['del_status' => '0','status' => 'active'])->orderBy(['id' => SORT_DESC])->limit(4)->all();
                    ?>

<div class="col-md-3 technology-right-1">
    <div class="blo-top">
        <div class="tech-btm">
        <img src="/images/newsletter.png" class="img-responsive" alt=""/>
        </div>
    </div>

    <div class="blo-top">
        <div class="tech-btm">
    <?php $form = ActiveForm::begin([
                 'id' => 'newsletter'
                ]); ?>
        <h4>Sign up to our newsletter</h4>
            <div class="name">
                
                <?= $form->field($signupMail, 'email')->textInput(['class' => 'form-control','placeholder' =>'Your Email']);?>
          
            </div>  
            <div class="button">
                    <input type="submit" value="Subscribe">
            </div>
            
        <div class="clearfix"> </div>
        <?php ActiveForm::end(); ?>
        </div>  
    </div>
    

    <div class="blo-top1">
        <div class="tech-btm">
        <h4>Top stories of the week </h4>
            <?php foreach($allPosts as $blog){
                $postImage = $postImages::find()->where(['post_id' => $blog->id])->one();
                ?>
            <div class="blog-grids">
                <div class="blog-grid-left">
                    <a href="/blog/post-detail?id=<?= $blog->url; ?>"><img src="<?= ($postImage) ? '/'.ImageUploader::resizeRender($postImage->url, '89', '85') : 'https://placeholdit.imgix.net/~text?txtsize=61&txt=282%C3%97300&w=89&h=85';?>" class="img-responsive" alt=""/></a>
                </div>
                <div class="blog-grid-right">
                    <h5><a href="/blog/post-detail?id=<?= $blog->url; ?>"><?= $blog->post_title;?></a> </h5>
                </div>
                <div class="clearfix"> </div>
            </div>
            <?php }?>
        </div>
    </div>
</div>
<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\MubUser;

AppAsset::register($this);

 $this->registerMetaTag([
      'title' => 'og:title',
      'content' =>'OSMSTAYS Property', 
   ]);

   $this->registerMetaTag([
      'app_id' => 'fb:app_id',
      'content' => '892362904261944'
   ]);

   $this->registerMetaTag([
      'type' => 'og:type',
      'content' => 'article'
   ]);

   $this->registerMetaTag([
      'url' => 'og:url',
      'content' => 'http://'.$_SERVER['HTTP_HOST'].\Yii::$app->request->url,
   ]);
      $this->registerMetaTag([
         'image' => 'og:image',
         'content' => 'http://'.$_SERVER['HTTP_HOST'].'/uploads/logo.jpg',
      ]);
   $this->registerMetaTag([
      'description' => 'og:description',
      'content' => 'Looking for a house on rent in Gurgoan? OSMSTAYS provide fully furnished/semi-furnished flats at an Affordable rate with no brokerage and no lock-in period.'
   ]);

/*close facebook meta tag*/

/*twitter meta tag*/

   $this->registerMetaTag([
      'card' => 'twitter:card',
      'content' => "summury"
   ]);
   $this->registerMetaTag([
      'site' => "twitter:site",
      'content' => "@publisher_handle"
   ]);
   
   $this->registerMetaTag([
      'title' => 'twitter:title',
      'content' => 'OSMSTAYS Property'
   ]);

   $this->registerMetaTag([
      'description' => 'twitter:description',
      'content' => 'Looking for a house on rent in Gurgoan? OSMSTAYS provide fully furnished/semi-furnished flats at an Affordable rate with no brokerage and no lock-in period.' 
   ]);

   $this->registerMetaTag([
      'creater' => 'twitter:creater',
      'content' => '@author_handle' 
   ]);

   $this->registerMetaTag([
      'url' => 'twitter:url',
      'content' => 'http://'.$_SERVER['HTTP_HOST'].\Yii::$app->request->url,
   ]);
   $this->registerMetaTag([
         'image' => 'twitter:image:src',
         'content' => 'http://'.$_SERVER['HTTP_HOST'].'/uploads/logo.jpg',
      ]);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en"><head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title>:: Electron Music Event Landing Page Template ::</title>
<meta name="description" content="Welcome to Electron Music Event Landing Page Template">
<meta name="keywords" content="music, event, enjoying, performer, guest, ticket, package, food, drink, seat, arrangement, guitar, live, concert ">
<meta name="author" content="Saptarang">

<!-- Mobile Specific Meta -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="/img/icons/favicon.ico">
<link rel="apple-touch-icon" href="/img/icons/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="72x72" href="/img/icons/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="/img/icons/apple-icon-114x114.png">
  <?= Html::csrfMetaTags() ?>
   <?php $this->head() ?>
</head>

<body onLoad="initialize()">
<div id="preloader">
  <img src="/img/Preloader.gif" alt="Preloader" />
</div>
<header id="top">
  <div class="container">
    <div class="row">
          <div class="col-md-3 col-sm-3 col-xs-6 logo">
                <a title="company Logo" href="index.html"><img class="img-responsive" src="/img/company.png" alt="Company Logo" /></a>
            </div>
          <div class="col-md-9 col-sm-8 col-xs-6 rightnav">
              <ul class="list-inline">
                    <li class="phone text-left"><i class="fa fa-phone round BGprime"></i>Call Us: <strong>312.555.1213</strong></li>
                </ul>
            </div>
        </div><!-- end row -->
  </div><!-- end container -->
  
    <!-- START NAVIGATION -->
    <nav class="nav">
        <ul class="list-unstyled main-menu">
          <!--Include your navigation here-->
          <li class="text-right"><a href="#" id="nav-close"><i class="fa fa-close"></i></a></li>
          <li><a href="#slides" title="Home"><i class="fa fa-home"></i> Home</a></li>
          <li><a href="#event" title="Event Info"><i class="fa fa-music"></i> Event</a></li>
          <li><a href="#performers" title="Special Guest"><i class="fa fa-star"></i> Special Guest</a></li>
          <li><a href="#artist" title="Team"><i class="fa fa-users"></i> Performers</a></li>
          <li><a href="#features" title="Features"><i class="fa fa-th"></i> Features</a></li>
          <li><a href="#offer" title="offer"><i class="fa fa-tags"></i> Offer</a></li>
          <li><a href="#pricing" title="Pricing"><i class="fa fa-dollar"></i> Pricing</a></li>
          <li><a href="#sponsors" title="Sponsors"><i class="fa fa-building"></i> Sponsors</a></li>
          <li><a href="#lineup" title="Lineup 2015"><i class="fa fa-calendar"></i> Lineup 2015</a></li>
          <li><a href="#about" title="About"><i class="fa fa-building"></i> About</a></li>
          <li><a href="#quotes" title="Testimonial"><i class="fa fa-quote-left"></i> Testimonial</a></li>
          <li><a href="#contact" title="Contact"><i class="fa fa-map-marker"></i> Contact</a></li>
        </ul>
    </nav>
              
    <div class="navbar navbar-inverse navbar-fixed-top">  
        <div class="navbar-header pull-right">
          <a id="nav-expander" class="nav-expander fixed">
            <i class="fa fa-bars fa-lg white"></i>
          </a>
        </div>
    </div>
    <!-- END NAVIGATION -->

</header>
<?= $content ?>
<section class="subscribe BGlight page-block">
  <div class="container">
    <div class="row">
    
      <div class="text-center">
        <h3 class="subscribeHeading">stay in touch for upcoming events</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.  Aenean viverra eget felis quis elementum. </p>
      </div>
      
      <div class="col-md-12 col-sm-12">
        <form action="form/subscribe.php" id="subscribeForm" method="post">
          <div class="form-row">
            <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
              <input type="email" id="emailSubscribe" name="emailSubscribe" class="form-control" placeholder="Enter Your Email Address Here..." />
              <input type="text" id="sub-security" name="sub-security" class="form-control hide" value="" />
              <input type="submit" id="sub-submit" name="sub-submit" class="sqaureIconPrime" value="&#xf067;" />
            </div>
          </div>
        </form>
      </div>
        
    </div><!-- end row -->
  </div><!-- end container -->
</section>
<!-- END  SUBSCRIBE -->

<!-- ::: BEGIN-VENUE ::: -->
<section id="venue" class="BGdark">
  <div class="container-wide">
    <div id="map_canvas"> </div>
  </div>
  <div class="container">
    <div class="row">
     
      <div class="col-md-12 text-center"><h2>Event Location</h2></div>
        
        <div class="col-md-3 col-sm-3">
          <div class="column-content">
            <ul class="address">
              <li> 
                  <i class="fa fa-map-marker"></i> <strong>Event Location</strong><br>
                  123 Main Street<br>
                  San Francisco, CA 94105, USA 
              </li>
              <li> 
                  <i class="fa fa-phone"></i> <strong>Call Us</strong><br>
                  310.55.1213 
              </li>
              <li> 
                  <i class="fa fa-envelope"></i> <strong>Email Us</strong><br>
                  <a href="mailto:info@yourdomain.com">info@yourdomain.com</a> 
              </li>
            </ul>
          </div>
        </div>
        
        <div class="col-md-9 col-sm-9">
          <div class="col-sm-12 directions-input">
            <h4>Get Directions</h4>
            <form action="/routebeschrijving" onSubmit="calcRoute();return false;" id="routeForm">
              <div class="col-sm-6">
                  <label for="routeStart"><strong>Destination From:</strong></label>
                  <input type="text" id="routeStart" placeholder="50 California Street">
              </div>
              <div class="col-sm-6">
                  <label for="routeVia"><strong>Via(Optional):</strong> </label>
                  <input type="text" id="routeVia" value="">
              </div>
              <div class="col-sm-7">
                <label>Travel mode:</label>
                <div class="clearfix">
                    <label class="radio-option" for="travelMode1">
                      <input type="radio" name="travelMode" id="travelMode1" value="DRIVING" checked />
                      Driving
                    </label>
                    <label class="radio-option" for="travelMode2">
                      <input type="radio" name="travelMode" id="travelMode2" value="BICYCLING" />
                      Bicylcing
                    </label>
                    <label class="radio-option" for="travelMode3">
                      <input type="radio" name="travelMode" id="travelMode3" value="TRANSIT" />
                      Public transport
                    </label>
                    <label class="radio-option last" for="travelMode4">
                      <input type="radio" name="travelMode" id="travelMode4" value="WALKING" />
                      Walking
                    </label>
                </div>
              </div>
              <div class="col-sm-5">
                <input type="submit" value="&#xf041; Get Directions" class="btn btn-primary">
              </div>
            </form>
        </div>
          
        <div class="col-sm-12 column directions-results">
          <div class="column-content">
            <h4>Directions</h4>
            <div id="directionsPanel"> Enter <strong>Destination From</strong> under <strong>Get Directions</strong> and Click on Get Directions Button. </div>
          </div>
        </div>
            
      </div>
    </div><!--end-of-row--> 
  </div>
  
</section>
<!-- ::: END MAP ::: -->

<!-- ::: START CONTACT ::: -->
<section id="contact" class="page-block BGsecondary">
  <div class="container">
     <div class="row">
     
        <div class="col-md-3 col-sm-12">
           <h2>Questions?</h2>
        </div>
        
        <div class="col-md-9 col-sm-12 social">
          <ul class="list-inline text-center">
            <li><a href="#" data-toggle="tooltip" title="Follow Us on Facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#" data-toggle="tooltip" title="Follow Us on Twitter"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#" data-toggle="tooltip" title="Watch Our Videos"><i class="fa fa-youtube-play"></i></a></li>
            <li><a href="#" data-toggle="tooltip" title="Follow Us On Pinterest"><i class="fa fa-pinterest-square"></i></a></li>
            <li><a href="#" data-toggle="tooltip" title="Follow Us on LinkedIn"><i class="fa fa-linkedin"></i></a></li>
            <li><a href="#" data-toggle="tooltip" title="Watch Our gallery"><i class="fa fa-flickr"></i></a></li>
            <li><a href="#" data-toggle="tooltip" title="Google Plus"><i class="fa fa-google-plus"></i></a></li>
            <li><a href="#" data-toggle="tooltip" title="Instagram"><i class="fa fa-instagram"></i></a></li>
          </ul>
        </div>
            
         <div class="col-md-12 col-sm-12">
            
            <div class="contactForm">
              <form id="contact_form" method="post" action="form/contact.php">
                <div class="row">
                <div class="form-row col-sm-4">
                  <input type="text" id="name" name="name" placeholder="Your Name" />
                </div>
                <div class="form-row col-sm-4">
                  <input type="tel" id="phone" name="phone" placeholder="Your Phone" />
                </div>
                <div class="form-row col-sm-4">
                  <input type="email" id="email" name="email" placeholder="Your Email" />
                </div>
                </div>
                <div class="form-row">
                  <textarea cols="60" rows="3" id="comment" name="comment" placeholder="Write your comment here..."></textarea>
                </div>
                <div class="form-row text-center">
                  <input type="text" id="security" name="security" class="hide" value="" />
                  <input type="submit" value="&#xf1d8; send message" class="btn btn-dark btn-lg" id="submit" name="submit" />
                </div>
              </form>
            </div>
            
      </div>
    </div><!-- end row -->
  </div><!-- end container -->
</section>
<!-- ::: END MAP ::: -->

<!-- ::: START COPYRIGHT ::: -->
<footer id="copyright" class="page-block-small BGprime">
  <div class="container text-center"> 
    <div class="row">
    
      <p>Electron &copy; 2015. All Rights Reserved.<br> Landing Page Template Designed &amp; Developed By: <a href="http://themeforest.net/user/saptarang?ref=saptarang" title="Saptarang"><strong>Saptarang</strong></a></p>
      
    </div><!-- end row -->
  </div><!-- end container -->
</footer>
<!-- ::: END MAP ::: -->

<a href="#slides" class="top"><i class="fa fa-angle-up fa-lg"></i></a>

 <script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
 ga('create', 'UA-104726724-1', 'auto');
 ga('send', 'pageview');
</script>
</body>
<?php $this->endBody();?>
</html>
<?php $this->endPage();?>

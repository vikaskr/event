<div class="banner banner-static">
			<div class="banner-cpn">
				<div class="container">
					<div class="content row">
					
						<div class="banner-text">
							<h1 class="page-title">Core3 Networks Services</h1>
							<p>Core3 offers Domestic & International termination services and direct routes for Telecom operators, resellers and companies.</p>						
						</div>
						<div class="page-breadcrumb">
							<ul class="breadcrumb">
								<li><a href="/">Home</a></li>
								<li class="active"><span>Our Services</span></li>
							</ul>
						</div>
						
					</div>
				</div>
			</div>
			<div class="banner-bg imagebg">
				<img src="/images/ser.jpg" alt="" />
			</div>
		</div>
		<!-- #end Banner/Static -->
	</header>
	<!-- End Header -->
	<!-- Content -->
	<div class="section section-content section-pad bg-light">
		<div class="container">
			<div class="row">
				
				<div class="row row-vm reverses">
					<div class="col-md-6 col-sm-6 res-m-bttm pad-r">
						<a href="#"><img class="img-shadow" src="/images/ser4.png" alt=""></a>
					</div>
					<div class="col-md-6 col-sm-6 pad-l">
						<h2 class="heading">Core3 Networks Services</h2>
						<p class="just">Core3 is one of the fastest growing International VoIP carrier in Hongkong.Core3 offers a complete portfolio of connections to Europe, North and South-America, Africa, Asia and the Middle-East. Core3 routes millions of international voice minutes from other carriers through state of the art switching platform. 
Core3 is known as the most aggressive, flexible and quality-driven company in the wholesale arena; we are always searching for VoIP and SS7-carriers offering good quality routes at the best tariffs available. Core3 maintains long term relationships with PTT's, incumbents and mobile operators from all over the world. We have interconnected with various global network providers through our points of presence and we are continually connecting more and more voice carriers to meet the demand of our partners.
</p>
						<!-- <p><a href="#" class="btn btn-outline">More about Services</a></p> -->
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
	<div class="section section-content section-pad">
		<div class="container">
			<div class="row">
			
				<div class="row row-vm">
					<div class="col-md-6 col-sm-6 res-m-bttm pad-r">
						<a href="#"><img class="img-shadow" src="/images/ser3.png" alt=""></a>
					</div>
					<div class="col-md-6 col-sm-6 pad-l">
						<h2 class="heading">Roaming Services</h2>
						<p> <b>Instant Roaming App</b><br/>
Simplify and accelerate roaming services with immediate access to our roaming App. In a matter of minutes, go roaming with two available options<br/><br/>

<b>Free Voice Or Free Data</b><br/>
With very low charges where you save 90% roaming cost while maintaining your own Mobile number during roaming.
</p>
</p><br/>
						<p><a href="/site/roaming" target="_blank" class="btn btn-outline">More about Services</a></p>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
	<div class="section section-content section-pad bg-light">
		<div class="container">
			<div class="row">
			
				<div class="row row-vm reverses">
					<div class="col-md-6 col-sm-6 res-m-bttm pad-r">
						<a href="#"><img class="img-shadow" src="/images/ser6.png" alt=""></a>
					</div>
					<div class="col-md-6 col-sm-6 pad-l">
						<h2 class="heading">International Calling</h2>
						<br/>
						<p>
This solution is available on Core3 seamless network, based on TDM and IP technologies. We have IDD and Hubbing direct routes with 15+ operators worldwide that allow A-Z destinations. These destinations are either fixed or mobile networks, providing premium quality termination. The International Direct Dialling solution provides:</p>
<br/><br/>
						<p><a href="/site/international" target="_blank" class="btn btn-outline">More about Services</a></p>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
	<div class="section section-content section-pad">
		<div class="container">
			<div class="row">
			
				<div class="row row-vm">
					<div class="col-md-6 col-sm-6 res-m-bttm pad-r">
						<a href="#"><img class="img-shadow" src="/images/ser5.png" alt=""></a>
					</div>
					<div class="col-md-6 col-sm-6 pad-l">
						<h2 class="heading">Hubbing Premium</h2>
						<p class="lead">
 This Premium voice transit service ensures the stability you need for your retail traffic :</br></p><p> 
1.)  Quality routing ensuring steady levels for Answer-to-Seizure Ratio</br>
2.)  Price stability</br>
3.)  Guaranteed transmission transparency for Calling Line Identification (CLI)</br>
4.)  100+ fixed and mobile destinations available worldwide with a premium high quality</br> 
5.)  Detailed and customized monthly QoS reporting</br>
6.)  24/7 enhanced fault management</br>
7.)  Innovative trouble tickets for signaling features</br>
</p>
						<!-- <p><a href="#" class="btn btn-outline">More about Services</a></p>
 -->					</div>
				</div>
				
			</div>
		</div>
	</div>
	
	<div class="section section-content section-pad bg-light">
		<div class="container">
			<div class="row">
			
				<div class="row row-vm reverses">
					<div class="col-md-6 col-sm-6 res-m-bttm pad-r">
						<a href="#"><img class="img-shadow" src="/images/ser2.png" alt=""></a>
					</div>
					<div class="col-md-6 col-sm-6 pad-l">
						<h2 class="heading">Hubbing Standard</h2>
						<p style="text-align: justify;">We help you optimize your costs while benefiting from a global coverage, dynamic routing and competitive prices. Hubbing Standard customers benefit from a price/quality ratio and find the flexibility they need to steer the best course of their business</p></br>
						<p>

1.)  Quality & coverage of Orange's network</br>
2.)  Market-driven pricing</br>
3.)  Routing based on an optimized price/quality ratio</br>
4.)  100+destinations available worldwide</br>
5.)	 Constant monitoring of the network
</p>
						<!-- <p><a href="#" class="btn btn-outline">More about Services</a></p> -->
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
	<!-- <div class="section section-content section-pad">
		<div class="container">
			<div class="row">
			
				<div class="row row-vm">
					<div class="col-md-6 col-sm-6 res-m-bttm pad-r">
						<a href="#"><img class="img-shadow" src="/images/ser.png" alt=""></a>
					</div>
					<div class="col-md-6 col-sm-6 pad-l">
						<h2 class="heading">Insurance Consulting</h2>
						<p class="lead">We provide best consulting sed do eiusmod tempor incididunt ut labore et dolore.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercation ullamco laboris nisi ution aliquip exon commodo conquat. Duis aute irure dolor nost.</p>
						<p><a href="#" class="btn btn-outline">More about Services</a></p>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
	<div class="section section-content section-pad bg-light">
		<div class="container">
			<div class="row">
			
				<div class="row row-vm reverses">
					<div class="col-md-6 col-sm-6 res-m-bttm pad-r">
						<a href="#"><img class="img-shadow" src="/images/ser.png" alt=""></a>
					</div>
					<div class="col-md-6 col-sm-6 pad-l">
						<h2 class="heading">Taxes consulting</h2>
						<p class="lead">Taxes consulting includes sed do eiusmod tempor incididunt ut labore et dolore.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercation ullamco laboris nisi ution aliquip exon commodo conquat. Duis aute irure dolor nost.</p>
						<p><a href="#" class="btn btn-outline">More about Services</a></p>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
	<div class="section section-content section-pad">
		<div class="container">
			<div class="row">
			
				<div class="row row-vm">
					<div class="col-md-6 col-sm-6 res-m-bttm pad-r">
						<a href="#"><img class="img-shadow" src="/images/ser.png" alt=""></a>
					</div>
					<div class="col-md-6 col-sm-6 pad-l">
						<h2 class="heading">Business Audit</h2>
						<p class="lead">We are specializes in business audit eiusmod tempor idunt labore et dolore.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercation ullamco laboris nisi ution aliquip exon commodo conquat. Duis aute irure dolor nost.</p>
						<p><a href="#" class="btn btn-outline">More about Services</a></p>
					</div>
				</div>
				
			</div>
		</div>
	</div> -->
	<!-- End Content -->	
	<!-- Client logo -->
	
<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = 'Register';
?>

  <div class="col-md-12 col-sm-12 col-xs-12">
        <h1 style="text-align: center">Sign Up </h1>
      </div>
      
        <?php $form = ActiveForm::begin([
              'id' => 'login-form',
              'class' => 'form-horizontal form-label-left',
              ]); ?>
          <div class="row">
          <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
            <?= $form->field($model, 'first_name')->textInput(['class' => 'form-control col-md-7 col-xs-12']);?>
          </div>
          <div class="col-md-5 col-sm-12 col-xs-12">
              <?= $form->field($model, 'last_name')->textInput(['class' => 'form-control col-md-7 col-xs-12']);?>
          </div>
          </div>
          <div class="row">
          <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
              <?= $form->field($model, 'email')->textInput(['class' => 'form-control col-md-7 col-xs-12']);?>
          </div>
          <div class="col-md-5 col-sm-12 col-xs-12">
              <?= $form->field($model, 'mobile')->textInput(['class' => 'form-control col-md-7 col-xs-12']);?>
          </div>
          </div>
          <div class="row">
          <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
              <?= $form->field($model, 'address')->textInput(['class' => 'form-control col-md-7 col-xs-12']);?>
          </div>
          <div class="col-md-5 col-sm-12 col-xs-12">
          <?= $form->field($model, 'domain')->textInput();?>
          </div>
          </div>
          <div class="row">
          <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
              <?= $form->field($model, 'state')->dropDownList($allStates, ['id' => 'mub-state','prompt' => 'Select A state']);?>
          </div>
          <div class="col-md-5 col-sm-12 col-xs-12">
              <?= $form->field($model, 'city')->dropDownList([], ['prompt' => 'Select A City' ,'id' =>'mubusercontact-city']);?>
          </div>
           </div>
           <div class="row">
           <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
              <?= $form->field($model, 'username')->textInput(['class' => 'form-control col-md-7 col-xs-12']);?>
          </div>
          <div class="col-md-5 col-sm-12 col-xs-12">
              <?= $form->field($model, 'organization')->textInput(['class' => 'form-control col-md-7 col-xs-12']);?>
          </div>
          </div>
          <div class="row">
          <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
              <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control col-md-7 col-xs-12']);?>
          </div>
          <div class="col-md-5 col-sm-12 col-xs-12">
              <?= $form->field($model, 'password_confirm')->passwordInput(['class' => 'form-control col-md-7 col-xs-12']);?>
          </div>
          </div>
          
          <div class="col-md-10 col-md-offset-1">
           <?= $form->field($model, 'user_states')->checkboxList($activeStates)->label("State of Operations (You can select Multiple)");?>
           </div>

         
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" style="text-align: center; margin-top: 1em;">
            <a href="/site/login" class="btn btn-primary">Back</a>
              <button type="submit" class="btn btn-success">Submit</button>
              
            </div>
          </div>
         <?php ActiveForm::end(); ?>
      </div>
    </div>
  </div>
</div>
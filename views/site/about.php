
<div class="banner banner-static">
			<div class="banner-cpn">
				<div class="container">
					<div class="content row">
					
						<div class="banner-text">
							<h1 class="page-title">About Core3 Networks</h1>
							<p>Core3 offers Domestic & International termination services and direct routes for Telecom operators, resellers and companies.  </p>						
						</div>
						<div class="page-breadcrumb">
							<ul class="breadcrumb">
								<li><a href="/">Home</a></li>
								<li class="active"><span>About Us</span></li>
							</ul>
						</div>
						
					</div>
				</div>
			</div>
			<div class="banner-bg imagebg">
				<img src="/images/192012.jpg" alt="" />
			</div>
		</div>
		<!-- #end Banner/Static -->
	</header>
	<!-- End Header -->
	<!-- Content -->
	<div class="section section-contents section-pad">
		<div class="container">
			<div class="content row row-vm">

				<div class="col-sm-7 npl pad-r res-m-bttm">
					<h2 class="heading-lg">About Us</h2>
					<p><strong>Core3 Network </strong>offers A-Z as well as direct routes for many countries worldwide. Our platform is backed by superior Billing System and competitive rates to assure your calls get connected with the highest quality of service in the market. 
We provide our clients a secured Web interface so they can view their usage, balance information and route statistics. Our system supports both H323 and SIP protocol and traffic is accepted from H323 and SIP Gateways, Soft switches, or any other 3rd party VoIP platform.</p>
				</div>
				<div class="col-sm-5 npr">
					<img class="img-shadow" alt="" src="/images/who.png">
				</div>

			</div>
		</div>
	</div>
	<!-- End content -->

	<!-- Why Choose -->
	<div class="section section-contents section-pad has-bg fixed-bg light bg-alternet">
		<div class="container">
			<div class="row">
				
				<div class="row">
					<div class="col-md-4 pad-r res-m-bttm">
						<h1 class="heading-lead">Why Choose Us</h1>
						<img src="/images/770.jpg" alt="">
					</div>
					<div class="col-md-8">
						<div class="row">
							<div class="col-sm-4 res-s-bttm">
								<div class="icon-box style-s4 photo-plx-full">
									<em class="fa fa-bar-chart-o" aria-hidden="true"></em>
								</div>
								
								<p>1. Over 100 direct routes with the best-in-class service quality</p>
							</div>
							<div class="col-sm-4">
								<div class="icon-box style-s4">
									<em class="fa fa-users" aria-hidden="true"></em>
								</div>
								
								<p>2. Support of different access types</p>
							</div>
							<div class="col-sm-4">
								<div class="icon-box style-s4">
									<em class="fa fa-users" aria-hidden="true"></em>
								</div>
															
								<p>3. Recognized for quality of service</p>
							</div>
							<div class="gaps size-lg"></div>
							<div class="col-sm-4 res-s-bttm">
								<div class="icon-box style-s4">
									<em class="fa fa-credit-card" aria-hidden="true"></em>
								</div>
								
								<p>4. Part of a top-3 international voice carrier's portfolio</p>
							</div>
							<div class="col-sm-4">
								<div class="icon-box style-s4">
									<em class="fa fa-trademark" aria-hidden="true"></em>
								</div>
								
								<p>5. Advanced and transparent web reporting on traffic and quality</p>
							</div>
							<div class="col-sm-4">
								<div class="icon-box style-s4">
									<em class="fa fa-trademark" aria-hidden="true"></em>
								</div>
								
								<p>6. 24/7 Customer service</p>
							</div>
							</div>
					</div>
				</div>
			</div>
		</div></div>
	<!-- End Why Choose -->

	<!-- Testimonials -->
	
	<!-- End Section -->
	<!-- Teams -->
	<div class="section section-teams section-pad">
		<div class="container">
			<div class="content row">

				<div class="row">
					<div class="col-md-10 col-md-offset-1 center">
						<h2 class="heading-lg">Our Executive Team</h2>
						<p>Lorem ipsum dolor sit amet consectetur to adipiscing elit labore et dolore. Eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minim veniam. Duis aute irure dolor nostrud ullamco.</p>
					</div>
				</div>
				<div class="team-member-row row">
					<div class="col-md-3 col-sm-6 col-xs-6 even">
						<!-- Team Profile -->
						<div class="team-member">
							<div class="team-photo">
								<img alt="" src="/images/540.jpg">
							</div>
							<div class="team-info">
								<h4 class="name">Robert Miller</h4>
								<p class="sub-title">Managing Director &amp; CEO</p>
							</div>
						</div>
						<!-- Team #end -->
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 odd">
						<!-- Team Profile -->
						<div class="team-member">
							<div class="team-photo">
								<img alt="" src="/images/540.jpg">
							</div>
							<div class="team-info">
								<h4 class="name">Stephen Everett</h4>
								<p class="sub-title">Chief Operating Officer</p>
							</div>
						</div>
						<!-- Team #end -->
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 even">
						<!-- Team Profile -->
						<div class="team-member">
							<div class="team-photo">
								<img alt="" src="/images/540.jpg">
							</div>
							<div class="team-info">
								<h4 class="name">Philip Hennessy</h4>
								<p class="sub-title">Non-Executive Director</p>
							</div>
						</div>
						<!-- Team #end -->
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 odd">
						<!-- Team Profile -->
						<div class="team-member">
							<div class="team-photo">
								<img alt="" src="/images/540.jpg">
							</div>
							<div class="team-info">
								<h4 class="name">Robert Miller</h4>
								<p class="sub-title">Head of Human Resources</p>
							</div>
						</div>
						<!-- Team #end -->
					</div>
				</div><!-- TeamRow #end -->
			</div>
		</div>
	</div>			
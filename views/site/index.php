<section id="slides">
  	<div class="slides-container">
  
      <div class="slide active">
        <div class="img"><img src="/img/slider/home-page-banner.jpg" alt="slide" class="img-responsive"></div>
          <div class="slide-caption">
            <div class="container">
              <div class="box">
                <h1>FEEL THE ENERGY</h1>
                <span>BREATHTAKING CROWD</span>
              </div>
            </div>
          </div>
      </div><!-- end slide1 -->
      
      <div class="slide">
        <div class="img"><img src="/img/slider/home-page-banner.jpg" alt="slide" class="img-responsive"></div>
          <div class="slide-caption">
            <div class="container">
              <div class="box">
                <h1>Shake it out</h1>
                <span>Rock solid sounds</span>
              </div>
            </div>
          </div>
      </div><!-- end slide2 -->
      
      <div class="slide">
        <div class="img"><img src="/img/slider/home-page-banner.jpg" alt="slide" class="img-responsive"></div>
          <div class="slide-caption">
            <div class="container">
              <div class="box">
                <h1>Electronic Madness</h1>
                <span>Thrilling performances</span>
              </div>
            </div>
          </div>
      </div><!-- end slide3 -->
      
  	</div><!-- end slides-container -->
 
    <div class="slides-navigation">
   <a class="prev sqaureIconSec" href="#"> <i class="fa fa-chevron-left"></i></a>
   <a class="next sqaureIconSec" href="#"> <i class="fa fa-chevron-right"></i></a>
  </div><!-- end slides-navigation -->
  
  <div class="shapes absShape">
      <div class="shape1 absShape"></div>
      <div class="shape2 absShape"></div>
     <!-- <div class="gradient absShape"></div>-->
  </div>
  <div class="container-fluid absShape">
      <div class="BGprime first col-md-6 col-sm-6 col-xs-12">
          <div class="countdown styled"></div>
      </div>
      <div class="BGsecondary col-md-6 col-sm-6 col-xs-12">
          <a class="btn join smooth" href="#pricing" title="Join the event">JOIN THE EVENT</a>
      </div>
  </div>
  
  <div class="holder"><i class="fa fa-chevron-down moreArrow moving"></i></div>
</section>
<!-- ::: END ::: -->

<!-- ::: START EVENT DATA ::: -->
<section id="event" class="eventData page-block BGdark text-center">
  <div class="container">
    <div class="row">

      <div class="col-md-6 col-sm-6 col-xs-12 prime">
         <div class="dblBorder">
             <div class="dblBorderChild BGdark">
                <span class="sqaureIconPrime absolute"><i class="fa fa-calendar prime"></i></span>
                <h3>Event Date &amp; Time</h3>
                <p class="big"><strong>7 August 2017</strong><br><span>@</span>10.00 pm</p>
             </div>
         </div>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12 secondary">
        <div class="dblBorder">
          <div class="dblBorderChild BGdark">
               <span class="sqaureIconSec absolute"><i class="fa fa-map-marker prime"></i></span> 
               <h3>Event Location</h3>
                <p class="big">123 Main Street<br>San Francisco, CA 94105, USA</p>
            </div>
         </div>
      </div>
      <div class="col-md-12 col-sm-12 col-xs-12">
      	<a class="btn smooth btn-lg" href="#venue" title="Get Directions"><i class="fa fa-arrow-circle-o-right"></i> Get Directions</a>
      </div>
  
    </div><!-- end row -->
  </div><!-- end container --> 
</section>
<!-- ::: END ::: -->

<!-- ::: START  ::: -->
<section class="page-block">
  <div class="container text-center">
  	 <div class="col-md-10 col-md-offset-1 col-sm-12">
        <h2>Best Music Event Ever</h2><hr>
        <p class="big">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer gravida velit quis dolor tristiqumsan. Pellentesque elit tortor, adipiscing vel velit in, ultricies fermentum nulla. Donec in urna sem. Nulla facilisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        <hr>
        <h5>Check it out</h5><br>
        <p><span class="sqaureIconSec"><i class="fa fa-chevron-down"></i></span></p>
    </div>
  </div><!-- end container -->
</section>
<!-- ::: END ::: -->

<!-- ::: START PERFORMERS ::: -->
<section id="performers" class="BGprime">

  <!-- SPECIAL GUEST -->
  <div class="container-wide guest">
    <div class="row">
    
        <div class="col-lg-6 col-lg-offset-6 col-md-5 col-md-offset-7 col-sm-12 col-xs-12 BGprime page-block-full">
        
          <h3 class="white">Special Guest Performance</h3>
          <h4><i class="fa fa-star"></i>Jenny Doe</h4>
          <strong>Grammy Award winner</strong>
          <hr class="light">
          <h4><i class="fa fa-star"></i>Jen Doe</h4>
          <strong>Grammy Award winner</strong>
          <hr class="light">
          <p class="white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer gravida velit quis dolor tristiqumsan. Pellentesque elit tortor, adipiscing vel velit in, ultricies fermentum nulla. Donec in urna sem. Nulla facilisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer gravida velit quis dolor tristiqumsan. Pellentesque elit tortor, adipiscing vel velit  in urna sem. Nulla facilisi.</p>
          
          <div class="sqaureIconPrime"><i class="fa fa-star"></i></div>
            
        </div>
        
    </div><!-- end row -->
  </div><!-- end container -->
  
  <!-- OTHER PERFORMERS --> 
  <section id="artist" class="experts BGdark">
    <div class="highlightBox">
      <div class="boxBg page-block">
        <div class="container">
          <div class="row">
          
              <div class="line"></div>
              
              <h2 class="white">EVENT PERFORMERS</h2>
              <div id="celebs" class="owl-carousel">
               <div class="item">
                  <div class="img">
                      <img src="/img/people/bride2.jpg" alt="Name" style="height: 315px;">
                  </div>
                  <div class="sqaureIconPrime"> <i class="fa fa-angle-down"></i> </div>
                  
                  <h4>DJ Jenny</h4>
                  <i class="fa fa-map-marker"></i> From Los Angeles, CA<br>
                  <i class="fa fa-music"></i> Gener: Rock!<br>
                  <ul class="list-inline">
                    <li><a href="#" title="Folow me on Facebook" target="_blank"><span class="sqaureIconSec"><i class="fa fa-facebook"></i></span></a></li>
                    <li><a href="#" title="Folow me on Twitter" target="_blank"><span class="sqaureIconSec"><i class="fa fa-twitter"></i></span></a></li>
                    <li><a href="#" title="Watch me on Youtube" target="_blank"><span class="sqaureIconSec"><i class="fa fa-youtube"></i></span></a></li>
                    <li><a href="#" title="SoundCloud" target="_blank"><span class="sqaureIconSec"><i class="fa fa-soundcloud"></i></span></a></li>
                  </ul>
               </div><!-- end -->
               
               <div class="item">
                  <div class="img">
                      <img src="/img/people/bride3.jpg" alt="Name">
                  </div>
                  <div class="sqaureIconPrime"> <i class="fa fa-angle-down"></i> </div>
                  
                  <h4>John Doe</h4>
                  <i class="fa fa-map-marker"></i> From Los Angeles, CA<br>
                  <i class="fa fa-music"></i> Gener: Rock!<br>
                  <ul class="list-inline">
                    <li><a href="#" title="Folow me on Facebook" target="_blank"><span class="sqaureIconSec"><i class="fa fa-facebook"></i></span></a></li>
                    <li><a href="#" title="Folow me on Twitter" target="_blank"><span class="sqaureIconSec"><i class="fa fa-twitter"></i></span></a></li>
                    <li><a href="#" title="Watch me on Youtube" target="_blank"><span class="sqaureIconSec"><i class="fa fa-youtube"></i></span></a></li>
                    <li><a href="#" title="SoundCloud" target="_blank"><span class="sqaureIconSec"><i class="fa fa-soundcloud"></i></span></a></li>
                  </ul>
               </div><!-- end -->
               
               <div class="item">
                  <div class="img">
                      <img src="/img/people/bride4.jpg" alt="Name">
                  </div>
                  <div class="sqaureIconPrime"> <i class="fa fa-angle-down"></i> </div>
                  
                  <h4>Jenny Doe</h4>
                  <i class="fa fa-map-marker"></i> From Los Angeles, CA<br>
                  <i class="fa fa-music"></i> Gener: Rock!<br>
                  <ul class="list-inline">
                    <li><a href="#" title="Folow me on Facebook" target="_blank"><span class="sqaureIconSec"><i class="fa fa-facebook"></i></span></a></li>
                    <li><a href="#" title="Folow me on Twitter" target="_blank"><span class="sqaureIconSec"><i class="fa fa-twitter"></i></span></a></li>
                    <li><a href="#" title="Watch me on Youtube" target="_blank"><span class="sqaureIconSec"><i class="fa fa-youtube"></i></span></a></li>
                    <li><a href="#" title="SoundCloud" target="_blank"><span class="sqaureIconSec"><i class="fa fa-soundcloud"></i></span></a></li>
                  </ul>
               </div><!-- end -->
               
               <div class="item">
                  <div class="img">
                      <img src="/img/people/bride5.jpg" alt="Name">
                  </div>
                  <div class="sqaureIconPrime"> <i class="fa fa-angle-down"></i> </div>
                  
                  <h4>DJ Pattrick</h4>
                  <i class="fa fa-map-marker"></i> From Los Angeles, CA<br>
                  <i class="fa fa-music"></i> Gener: Rock!<br>
                  <ul class="list-inline">
                    <li><a href="#" title="Folow me on Facebook" target="_blank"><span class="sqaureIconSec"><i class="fa fa-facebook"></i></span></a></li>
                    <li><a href="#" title="Folow me on Twitter" target="_blank"><span class="sqaureIconSec"><i class="fa fa-twitter"></i></span></a></li>
                    <li><a href="#" title="Watch me on Youtube" target="_blank"><span class="sqaureIconSec"><i class="fa fa-youtube"></i></span></a></li>
                    <li><a href="#" title="SoundCloud" target="_blank"><span class="sqaureIconSec"><i class="fa fa-soundcloud"></i></span></a></li>
                  </ul>
               </div><!-- end -->
               
               <div class="item">
                  <div class="img">
                      <img src="/img/people/05.jpg" alt="Name">
                  </div>
                  <div class="sqaureIconPrime"> <i class="fa fa-angle-down"></i> </div>
                  
                  <h4>Jen Doe</h4>
                  <i class="fa fa-map-marker"></i> From Los Angeles, CA<br>
                  <i class="fa fa-music"></i> Gener: Rock!<br>
                  <ul class="list-inline">
                    <li><a href="#" title="Folow me on Facebook" target="_blank"><span class="sqaureIconSec"><i class="fa fa-facebook"></i></span></a></li>
                    <li><a href="#" title="Folow me on Twitter" target="_blank"><span class="sqaureIconSec"><i class="fa fa-twitter"></i></span></a></li>
                    <li><a href="#" title="Watch me on Youtube" target="_blank"><span class="sqaureIconSec"><i class="fa fa-youtube"></i></span></a></li>
                    <li><a href="#" title="SoundCloud" target="_blank"><span class="sqaureIconSec"><i class="fa fa-soundcloud"></i></span></a></li>
                  </ul>
               </div><!-- end -->
               
               <div class="item">
                  <div class="img">
                      <img src="/img/people/06.jpg" alt="Name">
                  </div>
                  <div class="sqaureIconPrime"> <i class="fa fa-angle-down"></i> </div>
                  
                  <h4>John Doe</h4>
                  <i class="fa fa-map-marker"></i> From Los Angeles, CA<br>
                  <i class="fa fa-music"></i> Gener: Rock!<br>
                  <ul class="list-inline">
                    <li><a href="#" title="Folow me on Facebook" target="_blank"><span class="sqaureIconSec"><i class="fa fa-facebook"></i></span></a></li>
                    <li><a href="#" title="Folow me on Twitter" target="_blank"><span class="sqaureIconSec"><i class="fa fa-twitter"></i></span></a></li>
                    <li><a href="#" title="Watch me on Youtube" target="_blank"><span class="sqaureIconSec"><i class="fa fa-youtube"></i></span></a></li>
                    <li><a href="#" title="SoundCloud" target="_blank"><span class="sqaureIconSec"><i class="fa fa-soundcloud"></i></span></a></li>
                  </ul>
               </div><!-- end -->
               
               <div class="item">
                  <div class="img">
                      <img src="/img/people/07.jpg" alt="Name">
                  </div>
                  <div class="sqaureIconPrime"> <i class="fa fa-angle-down"></i> </div>
                  
                  <h4>DJ Jenny</h4>
                  <i class="fa fa-map-marker"></i> From Los Angeles, CA<br>
                  <i class="fa fa-music"></i> Gener: Rock!<br>
                  <ul class="list-inline">
                    <li><a href="#" title="Folow me on Facebook" target="_blank"><span class="sqaureIconSec"><i class="fa fa-facebook"></i></span></a></li>
                    <li><a href="#" title="Folow me on Twitter" target="_blank"><span class="sqaureIconSec"><i class="fa fa-twitter"></i></span></a></li>
                    <li><a href="#" title="Watch me on Youtube" target="_blank"><span class="sqaureIconSec"><i class="fa fa-youtube"></i></span></a></li>
                    <li><a href="#" title="SoundCloud" target="_blank"><span class="sqaureIconSec"><i class="fa fa-soundcloud"></i></span></a></li>
                  </ul>
               </div><!-- end -->
               
               <div class="item">
                  <div class="img">
                      <img src="/img/people/08.jpg" alt="Name">
                  </div>
                  <div class="sqaureIconPrime"> <i class="fa fa-angle-down"></i> </div>
                  
                  <h4>John Doe</h4>
                  <i class="fa fa-map-marker"></i> From Los Angeles, CA<br>
                  <i class="fa fa-music"></i> Gener: Rock!<br>
                  <ul class="list-inline">
                    <li><a href="#" title="Folow me on Facebook" target="_blank"><span class="sqaureIconSec"><i class="fa fa-facebook"></i></span></a></li>
                    <li><a href="#" title="Folow me on Twitter" target="_blank"><span class="sqaureIconSec"><i class="fa fa-twitter"></i></span></a></li>
                    <li><a href="#" title="Watch me on Youtube" target="_blank"><span class="sqaureIconSec"><i class="fa fa-youtube"></i></span></a></li>
                    <li><a href="#" title="SoundCloud" target="_blank"><span class="sqaureIconSec"><i class="fa fa-soundcloud"></i></span></a></li>
                  </ul>
               </div><!-- end -->
               
             </div><!-- end Carousel -->
          </div>
        </div>
      </div><!-- end row -->
    </div><!-- end container -->
  </section>
</section>
<!-- ::: END ::: -->
    
<!-- ::: START FEATURES ::: -->
<section id="features" class="features">
  <div class="container page-block text-center">
  	 <div class="col-md-10 col-md-offset-1 col-sm-12">
        <div class="row">
    		<h2>Highlights</h2><hr>
            <p class="big">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer gravida velit quis dolor tristiqumsan. Pellentesque elit tortor, adipiscing vel velit in, ultricies fermentum nulla. Donec in urna sem. Nulla facilisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
            <hr>
            <h5>Check it out</h5><br>
            <p><span class="sqaureIconSec"><i class="fa fa-chevron-down"></i></span></p>
        </div>
     </div>
  </div>
  <div class="highlightBox  BGdark">
     <div class="boxBg page-block text-center">
        <div class="container">
          <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="featureBox">
                    <div class="sqaureIconPrime absolute"><i class="fa fa-ticket"></i></div>
                    <h5>TICKETS</h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer gravida velit quis dolor tristique accumsan. Pellentesque elit tortor, adipiscing vel velit in,</p>
                </div>
            </div>
            
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="featureBox">
                    <div class="sqaureIconPrime absolute"><i class="fa fa-cutlery"></i></div>
                    <h5>FOOD N DRINKS</h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer gravida velit quis dolor tristique accumsan. Pellentesque elit tortor, adipiscing vel velit in,</p>
                </div>
            </div>
            
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="featureBox">
                    <div class="sqaureIconPrime absolute"><i class="fa fa-car"></i></div>
                    <h5>PARKING</h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer gravida velit quis dolor tristique accumsan. Pellentesque elit tortor, adipiscing vel velit in,</p>
                </div>
            </div>
            
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="featureBox">
                    <div class="sqaureIconPrime absolute"><i class="fa fa-users"></i></div>
                    <h5>INCREDIBLE CROWD</h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer gravida velit quis dolor tristique accumsan. Pellentesque elit tortor, adipiscing vel velit in,</p>
                </div>
            </div>
        
          </div><!-- end row -->
  		</div><!-- end container -->
        
      <span class="sqaureIconSec absolute more"><i class="fa fa-plus"></i></span> 
      
    </div>
  </div>
</section>
<!-- ::: END ::: -->

<!-- ::: START FEATURES ::: -->
<section class="BGlight">

	<!-- START TABS -->
	<div class="container page-block">
      
      <div class="tabs clearfix">
        <div class="col-md-4 col-sm-4 col-xs-12"> <!-- required for floating -->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs tabs-left">
              <li class="active"><a href="#01" data-toggle="tab"><span><i class="fa fa-map-marker"></i></span> Accessibility</a></li>
              <li><a href="#02" data-toggle="tab"><span><i class="fa fa-music"></i></span> Admission</a></li>
              <li><a href="#03" data-toggle="tab"><span><i class="fa fa-star"></i></span> Pricing</a></li>
              <li><a href="#04" data-toggle="tab"><span><i class="fa fa-hotel"></i></span> Accomodation</a></li>
              <li><a href="#05" data-toggle="tab"><span><i class="fa fa-car"></i></span> Parking</a></li>
              <li><a href="#06" data-toggle="tab"><span><i class="fa fa-user"></i></span> Dress Code</a></li>
              <li><a href="#07" data-toggle="tab"><span><i class="fa fa-glass"></i></span> Food &amp; Beverages</a></li>
              <li><a href="#08" data-toggle="tab"><span><i class="fa fa-bullhorn"></i></span> Rules &amp; Safety</a></li>
              <li><a href="#09" data-toggle="tab"><span><i class="fa fa-shopping-cart"></i></span> Shopping</a></li>
            </ul>
        </div>
            
        <div class="col-md-8 col-sm-8 col-xs-12">
            <!-- Tab panes -->
            <div class="tab-content">
                
                <div class="tab-pane active" id="01">
                  <h3><i class="fa fa-map-marker"></i>Accessibility</h3>
<p>Donec urna odio, dapibus cursus elit at, pulvinar hendrerit ligula. Phasellus at maximus mauris. Morbi quis imperdiet massa, a suscipit dui. Mauris porta interdum odio, eleifend vestibulum felis condimentum ut. Morbi tincidunt posuere accumsan. Quisque tristique lacinia sollicitudin. Maecenas viverra velit id bibendum volutpat. Phasellus iaculis consequat diam in pharetra. Suspendisse a commodo est, quis viverra libero. Ut faucibus rutrum</p>
<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam dapibus dolo Maecenas nec lorem arcu. Morbi arcu sapien, lobortis imperdiet</p>
                  <a class="btn btn-primary smooth" href="#venue" title="Get Directions"><i class="fa fa-arrow-circle-o-right"></i> Get Directions</a>
                  
                </div><!-- end -->
                
                <div class="tab-pane" id="02">
                  <h3><i class="fa fa-music"></i>Admission</h3>
<p>Donec urna odio, dapibus cursus elit at, pulvinar hendrerit ligula. Phasellus at maximus mauri  porta interdum odio, eleifend vestibulum felis condimentum ut. Morbi tincidunt posuere accumsan Phasellus iaculis consequat diam in phar </p> 
                  <ul class="list-default">
                      <li>Pellentesque habitant morbi tristique senectus</li>
                      <li>Aet netus et malesuada fames ac turpis egestas</li>
                      <li>Aliquam dapibus dolor mi, sed viverra orci ultrices eu</li>
                      <li>In cursus odio vitae mi dignissim maximus. Maecenas nec lorem arcu</li>
                      <li>God rbi arcu sapien, lobortis imperdiet egestas quis, aliquet vitae lacus</li>
                  </ul>
                </div><!-- end -->
                
                <div class="tab-pane" id="03">
                  <h3><i class="fa fa-star"></i>Pricing</h3>
                  <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam dapibus dolor mi, sed viverra orci ultrices eu. In cursus odio vitae mi dignissim maximus. Maecenas nec lorem arcu. Morbi arcu sapien, lobortis imperdiet egestas quis, aliquet vitae lacus. Cras porttitor placerat purus, sagittis convallis arcu pellentesque vel. Aenean in purus consequat, laoreet dui sed, mattis nibh. Praesent auctor neque ex, id tempus ante egestas ac.</p>
                  <a class="btn btn-primary" href="#pricing" title="Get Directions"><i class="fa fa-arrow-circle-o-right"></i> Pricing</a>
                </div><!-- end -->
                
                <div class="tab-pane" id="04">
                  <h3><i class="fa fa-hotel"></i>Accomodation</h3>
                  <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam dapibus dolor mi, sed viverra orci ultrices eu. In cursus odio vitae mi dignissim maximus. Maecenas nec lorem arcu. Morbi arcu sapien, lobortis imperdiet egestas quis, aliquet vitae lacus. Lorem ipsum dolor sit ame  eu.</p>
                  <p>In cursus odio vitae mi dignissim maximus. Maecenas nec lorem arcu. Morbi arcu sapien, lobortis imperdiet egestas quis, aliquet vitae lacus. Cras porttitor placerat purus, sagittis convallis arcu pellentesque vel. Aenean in purus consequat, laoreet dui sed, mattis nibh. Praesent auctor neque ex, id tempus ante egestas ac.</p>
                  <a class="btn btn-primary" href="#pricing" title="Check Hotel"><i class="fa fa-arrow-circle-o-right"></i> Check Hotel</a>
                </div><!-- end -->
                
                <div class="tab-pane" id="05">
                  <h3><i class="fa fa-car"></i>Parking</h3>
                  <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam dapibus dolor mi, sed viverra orci ultrices eu. In cursus odio vitae mi dignissim maximus. sagittis convallis arcu pellentesque vel. Aenean in purus consequat, laoreet dui sed, mattis nibh. Praesent auctor neque ex, id tempus ante egestas ac. </p>
                  <p>In cursus odio vitae mi dignissim maximus. Maecenas nec lorem arcu. Morbi arcu sapien, lobortis imperdiet egestas quis, aliquet vitae lacus. Cras porttitor placerat purus, sagittis convallis arcu pellentesque vel. Aenean in purus consequat, laoreet dui sed, mattis nibh. Praesent auctor neque ex, id tempus ante egestas ac.</p>
                </div><!-- end -->
                
                <div class="tab-pane" id="06">
                  <h3><i class="fa fa-user"></i>Dress Code</h3>
                  <p>In cursus odio vitae mi dignissim maximus. Maecenas nec lorem arcu. Morbi arcu sapien, lobortis imperdiet egestas quis, aliquet vitae lacus. Cras porttitor placerat purus, sagittis convallis arcu pellentesque vel. Aenean in purus consequat, laoreet dui sed, mattis nibh. Praesent auctor neque ex, id tempus ante egestas ac.</p>
                  <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam dapibus dolor mi, sed viverra orci ultrices eu. In cursus odio vitae mi dignissim maximus. Maecenas nec lorem arcu. Morbi arcu sapien, lobortis imperdiet egestas quis Praesent auctor neque ex, id tempus ante egestas ac.</p>
                </div><!-- end -->
                
                <div class="tab-pane" id="07">
                  <h3><i class="fa fa-glass"></i>Food &amp; Beverages</h3>
                  <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam dapibus dolor mi, sed viverra orci ultrices eu. In cursus odio vitae mi dignissim maximus. Maecenas nec lorem arcu. Morbi arcu sapien, lobortis imperdiet egestas quis</p>
                  <p>In cursus odio vitae mi dignissim maximus. Maecenas nec lorem arcu. Morbi arcu sapien, lobortis imperdiet egestas quis, aliquet vitae lacus. Cras porttitor placerat purus, sagittis convallis arcu pellentesque vel. Aenean in purus consequat, laoreet dui sed, mattis nibh. Praesent auctor neque ex, id tempus ante egestas ac.</p>
                </div><!-- end -->
                
                <div class="tab-pane" id="08">
                  <h3><i class="fa fa-bullhorn"></i>Rules &amp; Safety</h3>
                  <p> Lorem ipsum dolor sit amet, consectetur adipiscing eli  cursus odio vitae mi dignissim maximus. Maecenas nec lorem arcu. Morbi arcu sapien, lobortis imperdiet egestas quis, aliquet vitae lacus. Cras porttitor placerat purus, sagittis convallis arcu pellentesque vel.</p>
                  <ul class="list-default">
                      <li>Pellentesque habitant morbi tristique senectus</li>
                      <li>Aet netus et malesuada fames ac turpis egestas</li>
                      <li>Aliquam dapibus dolor mi, sed viverra orci ultrices eu</li>
                      <li>In cursus odio vitae mi dignissim maximus. Maecenas nec lorem arcu</li>
                      <li>God rbi arcu sapien, lobortis imperdiet egestas quis, aliquet vitae lacus</li>
                  </ul>
                </div><!-- end -->
                
                <div class="tab-pane" id="09">
                  <h3><i class="fa fa-shopping-cart"></i>shopping</h3>
                  <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                  <ul class="list-default">
                      <li>Pellentesque habitant morbi tristique senectus</li>
                      <li>Aet netus et malesuada fames ac turpis egestas</li>
                      <li>Aliquam dapibus dolor mi, sed viverra orci ultrices eu</li>
                      <li>In cursus odio vitae mi dignissim maximus. Maecenas nec lorem arcu</li>
                      <li>God rbi arcu sapien, lobortis imperdiet egestas quis, aliquet vitae lacus</li>
                  </ul>
                </div><!-- end -->
                  
            </div><!-- end-tab-content -->
          </div>
       </div><!-- end-tabs -->
    </div><!-- end-container -->
    
</section>
<!-- ::: END ::: -->

<!-- ::: OFFERS ::: -->
<section id="offer" class="offer">
  <div class="container page-block">
    <div class="row ">
        
      <div class="col-lg-4 col-md-5 col-sm-5 col-xs-12">
        <h1>30</h1><h1 class="small">%<span>OFF</span></h1>
      </div>
      
      <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
        <h3>Purchase tickets before 20th MARCH</h3>
        <p class="big">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer gravida velit quis dolor tristiqumsan. Pellentesque elit tortor, adipiscing vel velit in, ultricies fermentum nulla. Donec in urna sem. Nulla facilisi.</p>
        <a class="btn btn-default smooth" href="#pricing" title="Purchase Tickets"><i class="fa fa-ticket"></i> Purchase Tickets</a>
      </div>
          
    </div><!-- end row -->
  </div><!-- end container -->
</section>
<!-- ::: END ::: -->

<!-- ::: START PRICING ::: -->
<section id="pricing" class="page-block BGdark">
  <div class="container">
    <div class="row">
        
      <h2>Pricing</h2>
      <div class="col-md-10 col-md-offset-1 col-sm-12">
          <p class="big text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean viverra eget felis quis elementum. Aliquam mollis vel tellus eget malesuada. Nulla euismod velit sit amet eros dictum tempor. </p>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-4 col-sm-12 col-xs-12">
        <div class="package BGdark">
          <div class="inner">
            <h2><sup>$</sup>499</h2>
            <h6>Group of 25</h6>
            <hr>
            <p class="small">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed risus tellus, volutpat quis, mollis auctor nisl. </p>
            <a class="btn btn-primary btn-sm" href="#pricing" title="Purchase Tickets"><i class="fa fa-ticket"></i> Purchase Tickets</a>
          </div>
        </div>
      </div>
      
      <div class="col-md-4 col-sm-12 col-xs-12">
        <div class="package BGprime">
          <div class="inner">
            <h2><sup>$</sup>999</h2>
            <h6>Group of 75</h6>
            <hr>
            <p class="small">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed risus tellus, volutpat quis, mollis auctor nisl. </p>
            <a class="btn btn-dark btn-sm" href="#pricing" title="Purchase Tickets"><i class="fa fa-ticket"></i> Purchase Tickets</a>
          </div>
        </div>
      </div>
      
      <div class="col-md-4 col-sm-12 col-xs-12">
        <div class="package BGdark">
          <div class="inner">
            <h2><sup>$</sup>799</h2>
            <h6>Group of 50</h6>
            <hr>
            <p class="small">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed risus tellus, volutpat quis, mollis auctor nisl. </p>
            <a class="btn btn-primary btn-sm" href="#pricing" title="Purchase Tickets"><i class="fa fa-ticket"></i> Purchase Tickets</a>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>      
    </div><!-- end row -->
  </div><!-- end container -->
</section>
<!-- ::: END ::: -->

<!-- ::: START MAP ::: -->
<section id="sponsors" class="sponsors page-block BGlight">
  <div class="container">
    <div class="row">
        <h2>sponsors</h2>
        <div class="col-sm-12">
           <div id="sponsorOwl" class="owl-carousel">
             
             <div class="item">
                <img src="/img/sponsors/01.jpg" alt="Name">
             </div>
             
             <div class="item">
                <img src="/img/sponsors/02.jpg" alt="Name">
             </div>
             
             <div class="item">
                <img src="/img/sponsors/03.jpg" alt="Name">
             </div>
             
             <div class="item">
                <img src="/img/sponsors/04.jpg" alt="Name">
             </div>
             
             <div class="item">
                <img src="/img/sponsors/05.jpg" alt="Name">
             </div>
             
             <div class="item">
                <img src="/img/sponsors/06.jpg" alt="Name">
             </div>
             
             <div class="item">
                <img src="/img/sponsors/07.jpg" alt="Name">
             </div>
             
             <div class="item">
                <img src="/img/sponsors/01.jpg" alt="Name">
             </div>
              
         </div>
       </div>
    </div><!-- end row -->
  </div><!-- end container -->
</section>
<!-- ::: END MAP ::: -->

<!-- ::: START LINEUP AND SUBSCRIBE ::: -->
<section id="lineup" class="page-block">

  <!-- OTHER LINEUP EVENTS -->
  <div class="container-wide">
    <div class="row">
        
      <h2>Lineup <span>2015</span></h2>
      <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
        <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean viverra eget felis quis elementum. Aliquam mollis vel tellus eget malesuada. Nulla euismod velit sit amet eros dictum tempor. </p>
      </div>
      <div class="clearfix"></div>
      <div id="upcoming" class="owl-carousel">
           
         <div class="item">
            <div class="img">
                <img src="/img/lineup/01.jpg" alt="Name">
                <a class="sqaureIconPrime" href="#" title="Name"> <i class="fa fa-link"></i> </a>
            </div>
            <div class="info">
                <h6>Lorem Ipsum Music Buzz </h6>
                <i class="fa fa-calendar"></i> 10 Jan, 2016
            </div>
         </div><!-- end -->
         
         <div class="item">
            <div class="img">
                <img src="/img/lineup/02.jpg" alt="Name">
                <a class="sqaureIconPrime" href="#" title="Name"> <i class="fa fa-link"></i> </a>
            </div>
            <div class="info">
                <h6>Lorem Ipsum Music Buzz </h6>
                <i class="fa fa-calendar"></i> 10 Jan, 2016
            </div>
         </div><!-- end -->
         
         <div class="item">
            <div class="img">
                <img src="/img/lineup/03.jpg" alt="Name">
                <a class="sqaureIconPrime" href="#" title="Name"> <i class="fa fa-link"></i> </a>
            </div>
            <div class="info">
                <h6>Lorem Ipsum Music Buzz </h6>
                <i class="fa fa-calendar"></i> 10 Jan, 2016
            </div>
         </div><!-- end -->
         
         <div class="item">
            <div class="img">
                <img src="/img/lineup/04.jpg" alt="Name">
                <a class="sqaureIconPrime" href="#" title="Name"> <i class="fa fa-link"></i> </a>
            </div>
            <div class="info">
                <h6>Lorem Ipsum Music Buzz </h6>
                <i class="fa fa-calendar"></i> 10 Jan, 2016
            </div>
         </div><!-- end -->
         
         <div class="item">
            <div class="img">
                <img src="/img/lineup/05.jpg" alt="Name">
                <a class="sqaureIconPrime" href="#" title="Name"> <i class="fa fa-link"></i> </a>
            </div>
            <div class="info">
                <h6>Lorem Ipsum Music Buzz </h6>
                <i class="fa fa-calendar"></i> 10 Jan, 2016
            </div>
         </div><!-- end -->
         
         <div class="item">
            <div class="img">
                <img src="/img/lineup/06.jpg" alt="Name">
                <a class="sqaureIconPrime" href="#" title="Name"> <i class="fa fa-link"></i> </a>
            </div>
            <div class="info">
                <h6>Lorem Ipsum Music Buzz </h6>
                <i class="fa fa-calendar"></i> 10 Jan, 2016
            </div>
         </div><!-- end -->
         
         <div class="item">
            <div class="img">
                <img src="/img/lineup/07.jpg" alt="Name">
                <a class="sqaureIconPrime" href="#" title="Name"> <i class="fa fa-link"></i> </a>
            </div>
            <div class="info">
                <h6>Lorem Ipsum Music Buzz </h6>
                <i class="fa fa-calendar"></i> 10 Jan, 2016
            </div>
         </div><!-- end -->
         
         <div class="item">
            <div class="img">
                <img src="/img/lineup/08.jpg" alt="Name">
                <a class="sqaureIconPrime" href="#" title="Name"> <i class="fa fa-link"></i> </a>
            </div>
            <div class="info">
                <h6>Lorem Ipsum Music Buzz </h6>
                <i class="fa fa-calendar"></i> 10 Jan, 2016
            </div>
         </div><!-- end -->
           
      </div><!-- end-carousel -->
              
     </div><!-- end row -->
    </div><!-- end container -->

</section>
<!-- ::: END ::: -->

<!-- ::: START ABOUT ::: -->
<section id="about" class=" page-block">
  <div class="container">
    <div class="row">        
        <div class="col-md-6 col-md-offset-6 col-sm-8 col-sm-offset-4 col-xs-12">
          <h2>About us</h2>
          <p class="big">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet ex consequat eros lacinia maximus eu sit amet est. Maecenas id risus accumsan, molestie lorem eu, facilisis quam. Vestibulum lacus arcu, cursus vitae elit facilisis, eleifend dignissim magna. Suspendisse luctus dignissim dui, tempus elementum magna venenatis eu. Sed nec vestibulum anteed tortor sem hyn luctus dignis tincidunt et porta dunt et porta dunt et porta vitae, accumsan in dolor.</p>
          <a class="btn btn-default" href="#" title="Company Name"><i class="fa fa-globe"></i> Visit Our Website</a>
        </div>  
  	</div><!-- end row -->
  </div><!-- end container -->
</section>
<!-- ::: END ::: -->

<!-- ::: START TESTIMONIALS ::: -->
<section id="quotes" class="page-block BGdark text-center">
  <div class="container">
    <div class="row">
    
      <div class="sqaureIconSec"><i class="fa fa-quote-left"></i></div>
      
      <div id="testimonial" class="carousel slide carousel-fade" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner text-center">
          
        <div class="item active">
            <h4>* EXTRAORDINARY PERFORMANCES *</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer gravida velit quis dolor tristique accumsan. Pellentesque elit tortor, adipiscing vel velit inermentum nulla. Donec in urna semulla facilisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tincidunt tellus vel libero efficitur vehicula Duis molestie tincidunt pellentes Proin elit turpis, blandit in libero in</p>
            <span><strong>John Doe</strong>Business Executive</span>
        </div><!-- end item -->
        
        <div class="item ">
            <h4>* GREAT MUSIC *</h4>
              <p> Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam at mauris id massa gravida pulvinar vel id ex. Integer gravida velit quis dolor tristique accumsan. Pellentesque elit tortor, adipiscing vel velit inermentum nulla. Donec in urna semulla facilisi. Liquam at mauris id massa gravida pulvinar</p>
              <span><strong>Richard Simon</strong>CEO at IT Company</span>
           
        </div><!-- end item -->
        
        <div class="item ">
            <h4>* AWESOME ARRANGEMENT *</h4>
            <p>Lliquam at mauris id massa gravida pulvinar Integer gravida velit quis dolor tristique accumsan. Pellentesque elit tortor, adipiscing vel velit inermentum nulla. Donec in urna semulla facilisi.  Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam at mauris id massa gravida pulvinar vel id ex.</p>
            <span><strong>Natasha Romonoff</strong>Agent</span>
            
        </div><!-- end item -->
          
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#testimonial" data-slide-to="0" class="active"></li>
          <li data-target="#testimonial" data-slide-to="1"></li>
          <li data-target="#testimonial" data-slide-to="2"></li>
        </ol>
              
        </div><!-- end carousel-inner -->
     </div><!-- end testimonial -->
    </div><!-- end-row -->
  </div><!-- end-container -->
</section>
<div class="banner banner-static">
			<div class="banner-cpn">
				<div class="container">
					<div class="content row">
					
						<div class="banner-text">
							<h1 class="page-title">Term & Condition</h1>					
						</div>
						<div class="page-breadcrumb">
							<ul class="breadcrumb">
								<li><a href="/">Home</a></li>
								<li class="active">Term & Condition</li>
								
							</ul>
						</div>
						
					</div>
				</div>
			</div>
			<div class="banner-bg imagebg">
				<img src="/images/192012.jpg" alt="" />
			</div>
		</div>
		<!-- #end Banner/Static -->
	</header>
<div class="section section-contents section-pad">
		<div class="container">
			<div class="content row">
			
				<div class="row">
				<div class="col-md-1"></div>
					<div class="col-md-10">						
						<h3 class="color-primary"><b>1. Term of Use - Unlawful or Unacceptable Use of the Core3 Networks Pvt. Ltd.</b></h3>
						<p class="just">You may not use, or permit the use of, the Core3 for unlawful purposes or for purposes that Core3 finds unacceptable. In order to fulfill this obligation, you may not transmit, post or receive certain material, which include but are not limited to the following: threatening, abusive, libelous, defamatory, obscene, pornographic, profane, or otherwise objectionable information of any kind, including without limitation any transmissions constituting or encouraging conduct that would result in a criminal offense or civil liability, or otherwise violate any local, state, national or international laws or regulations. Moreover, you may not (i) Transmit any information or software which contains a virus, worm, Trojan Horse, or other harmful component; (ii) Transmit any information, software or other material that is protected by copyright or other proprietary right (including trade secret materials), or derivative works thereof, without obtaining permission of the copyright owner or right holder; (iii) Transmit any bulk email, whether or not solicited; or (iv) Transmit any unsolicited bulk email (also known as "spam"). You agree to indemnify Core3 and hold Core3 harmless from any and all claims, damages, losses, and expenses (including attorney's fees and expenses) resulting from or allegedly resulting from your use of the Core3 whether or not such use is found to be in violation of any statute, rule or regulation. IMPORTANT NOTE: IF YOU VIOLATE THIS ACCEPTABLE USE POLICY, YOUR ACCOUNT MAY BE SUSPENDED OR TERMINATED WITHOUT NOTICE, AND WITHOUT A REFUND, AT THE DISCRETION OF Core3. </p>
						<!-- Acorrdion Panels -->
						<div class="panel-group accordion" id="general" role="tablist" aria-multiselectable="true">
							<!-- each panel -->
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="ques-i1">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#general" href="#ques-ans-i1" aria-expanded="false">
											<h3 class="color-primary"><b>2. Term of Use - Access to Other Networks.</b></h3>
											<span class="plus-minus"><span></span></span>
										</a>
									</h4>
								</div>
								<div id="ques-ans-i1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="ques-i1">
									<div class="panel-body">
										  <p>Any time you access another network through the Core3 Service, you must comply with that network's rules and policies. You agree to indemnify Core3 and hold Core3 harmless from any and all claims, damages, losses, and expenses (including attorney's fees and expenses) resulting from or allegedly resulting from your access or use of other networks.</p>
									</div>
								</div>
							</div> 
							<!-- each panel -->
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="ques-i2">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#general" href="#ques-ans-i2" aria-expanded="false">
											<h3 class="color-primary"><b>3. Term of Use - Control Over Content.</b></h3>
											<span class="plus-minus"><span></span></span>
										</a>
									</h4>
								</div>
								<div id="ques-ans-i2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="ques-i2">
									<div class="panel-body">
										<p>Core3 provides access to various forms of content that are available over the Internet. Core3 does not screen in advance any specific information available over the Internet. Core3 reserves the right (but does not assume the responsibility) to block or limit access to general categories of content that Core3, Inc deems in its sole discretion to be harmful, offensive, or otherwise in violation of this Core3 AUP. Core3 shall have no liability for any action or inaction with respect to content received over the Internet. It is your responsibility to control access to information that you might find unsuitable. This includes controlling access by minors through your account. By signing up for the Core3 Service you acknowledge receiving this warning and will not hold Core3 responsible for language, opinions, discussions or graphics which may be viewed on the Internet. Core3 has no obligation to monitor the Internet or any service offered via the Internet, including an Core3, Inc Service; however, you agree that Core3 has the right to monitor electronically, from time to time, and to disclose any information as Core3 in its sole discretion deems necessary to satisfy any law, to operate its Services properly, or to protect itself or its subscribers.</p>
									</div>
								</div>
							</div>
							<!-- each panel -->
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="ques-i3">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#general" href="#ques-ans-i3" aria-expanded="false">
										   <h3 class="color-primary"><b>4. Term of Use - Liability Limitations.</b></h3>
											<span class="plus-minus"><span></span></span>
										</a>

									</h4>
								</div>
								<div id="ques-ans-i3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="ques-i3">
									<div class="panel-body">
										<p>Core3, Inc MAKES NO WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, FOR THE SERVICES IT IS PROVIDING. Core3 DISCLAIMS ALL WARRANTIES, INCLUDING BUT NOT LIMITED TO ANY WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. Core3 WILL NOT BE RESPONSIBLE FOR ANY DAMAGE SUFFERED BY THE CUSTOMER, INCLUDING LOSS OF DATA RESULTING FROM DELAYS, FAILURE TO DELIVER, MISTAKEN DELIVERIES OR SERVICE INTERRUPTIONS. THE USE OF ANY INFORMATION OBTAINED THROUGH THE Core3, Inc SERVICES IS AT YOUR OWN RISK. Core3 MAKES NO WARRANTY AS TO THE CONTENT OR ACCURACY OF THE INFORMATION. Core3, Inc IS NOT LIABLE FOR ANY CLAIMS RESULTING FROM ANY FAILURE OR INTERRUPTION OF THE SERVICES. Core3 WILL, HOWEVER, REPAY YOU (IF YOU HAVE ALREADY PAID) FOR ANY PERIOD IN WHICH THE SERVICES WERE NOT PROVIDED.</p>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="ques-i4">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#general" href="#ques-ans-i4" aria-expanded="false">
										   <h3 class="color-primary"><b>5. Term of Use - Miscellaneous. </b></h3>
											<span class="plus-minus"><span></span></span>
										</a>

									</h4>
								</div>
								<div id="ques-ans-i4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="ques-i4">
									<div class="panel-body">
										<p>Core3, Inc reserves the right to modify this Acceptable Use Policy from time to time, which shall become effective upon posting to the Core3 Website or its successor site. Continued use of an Core3 Service constitutes acceptance of modifications made to this Acceptable Use Policy. In addition to any other available remedies, Core3 may suspend your access to and/or terminate your account without refund for any Core3 Service immediately and without notice if you engage in any conduct or activities that Core3, in its sole discretion, believes are in violation of this Acceptable Use Policy. If you engage in any activities which cause damage to Core3, its customers, or its suppliers, Core3 reserves the right to pursue a claim against you, including, but not limited to, a claim for monetary damages to address economic losses that may occur. Nothing contained in this Acceptable Use Policy shall be construed to limit the rights and remedies of Core3 and Core3, hereby reserves all such rights and remedies which may be available to it at law or in equity.</p>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="ques-i5">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#general" href="#ques-ans-i5" aria-expanded="false">
										 <h3 class="color-primary"><b>6. Term of Use - Enforceability.</b></h3>
											<span class="plus-minus"><span></span></span>
										</a>

									</h4>
								</div>
								<div id="ques-ans-i5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="ques-i5">
									<div class="panel-body">
										<p>In the event that any portion of this Acceptable Use Policy is held to be unenforceable, the unenforceable portion shall be construed in accordance with applicable law as nearly as possible to reflect the original intentions of the parties, and the remainder of the provisions shall remain in full force and effect. Any failure by Core3 to insist upon or enforce strict performance of any provision of this Acceptable Use Policy shall not be construed as a waiver of any provision or right. This Acceptable Use Policy shall be governed by and construed in accordance with the laws of the State of Illinois, without regard to its conflicts of law provisions. Any cause of action you may have with respect to the Core3, Service must be commenced within one (1) year after the claim or cause of action arises or such claim or cause of action is barred. YOU ACKNOWLEDGE THAT YOU HAVE READ THIS ACCEPTABLE USE POLICY, UNDERSTAND IT, AND AGREE TO BE BOUND BY ITS TERMS AND CONDITIONS.</p>
									</div>
								</div>
							</div>
							<!-- each panel -->
						</div>
						<!-- End Acorrdion -->
						
						<h3 class="color-secondary">Areas of Expertise</h3>
						<p>Bring to the table dolor sit amet enim ad minim veniam, quis nostrud exercation ullamco laboris nisi ution aliquip exon commodo conquat. Duis aute irure dolor nostrud ullamco. Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						<p>If you have any questions regarding our services, please <strong>contact us</strong> or call at <strong>800 1234 5677</strong>.</p>
						
					</div>
					
					<!-- Sidebar -->
					<!-- <div class="col-md-4">
						<div class="sidebar-right">

							<div class="wgs-box wgs-menus">
								<div class="wgs-content">
									<ul class="list list-grouped">
										<li class="list-heading">
											<span>Our Solutions and Services</span>
											<ul>
												<li><a href="service-single.html">Loan Sanction</a></li>
												<li class="active"><a href="service-single-alter.html">Investment Planning</a></li>
												<li><a href="service-single.html">Insurance Consulting</a></li>
												<li><a href="service-single-alter.html">Taxes Consulting</a></li>
												<li><a href="service-single.html">Business Audit</a></li>
												<li><a href="service-single-alter.html">Mutual Funds</a></li>
											</ul>
										</li>
									</ul>									
								</div>
							</div>
							
							<div class="wgs-box boxed light has-bg">
								<div class="wgs-content">
									<h3>Innovative Tools for Investor</h3>
									<p>We employ a long-established strategy, sector-focused investing across all of our markets globally...</p>
									<a href="service-single.html" class="btn btn-alt btn-outline"> Learn More</a>
								</div>
								<div class="wgs-bg imagebg">
									<img src="/images/photo-sd-a.jpg" alt="">
								</div>
							</div>
							
							<div class="wgs-box boxed bg-secondary light">
								<div class="wgs-content">
									<h3>Need Help To Grow Your Business?</h3>
									<p>Investment Expert will help you start your own company.</p>
									<a href="contact.html" class="btn btn-light"> Get In Touch</a>
								</div>
							</div>

						</div>
					</div> -->
					<!-- Sidebar #end -->
				</div>
				
			</div>
		</div>		
	</div>